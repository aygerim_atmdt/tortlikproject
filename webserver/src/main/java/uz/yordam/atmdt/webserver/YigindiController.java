package uz.yordam.atmdt.webserver;

import io.vertx.ext.web.RoutingContext;

public class YigindiController {
    public void yigindi(RoutingContext context) {
        String son1 = context.request().params().get("son1");
        String son2 = context.request().params().get("son2");
        int s1 = Integer.parseInt(son1);
        int s2 = Integer.parseInt(son2);
        int son = s1 + s2;
        context.response().end("yigindi" + s1 + "+" + s2 + "=" + son);
    }
}

