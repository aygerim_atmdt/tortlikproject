package uz.yordam.atmdt.webserver;


import io.vertx.core.Vertx;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;


public class MainRouter {
    private MainController mainController;
    private SevimliController sevimliController;
    private GuruxController guruxController;
    private AygerimController aygerimController;
    private ChiroyliController chiroyliController;
    private NoadatiyController noadatiyController;
    private HayotiyController hayotiyController;
    private YigindiController yigindiController;
    private dynamicController dinamikController;
    private archaController archaController;
    private AyirmaController ayirmaController;
    private StipendiyaController stipendiyaController;

    public MainRouter(){
        mainController=new MainController();
        sevimliController=new SevimliController();
        guruxController =new GuruxController();
        aygerimController=new AygerimController();
        chiroyliController=new ChiroyliController();
        noadatiyController=new NoadatiyController();
        hayotiyController=new HayotiyController();
        yigindiController=new YigindiController();
        dinamikController=new dynamicController();
        archaController=new archaController();
        ayirmaController=new AyirmaController();
        stipendiyaController=new StipendiyaController();
    }
    public Router getRouter(Vertx vertx){
        Router router=Router.router(vertx);
        router.route("/duxi").handler(mainController::Duxi);
        router.route("/salom").handler(aygerimController::Salom);
        router.route("/ayko").handler(mainController::atir);
        router.route("/sher").handler(aygerimController::Ayko);
        router.route("/lola").handler(sevimliController::Lola);
        router.route("/atirgul").handler(sevimliController::Atirgul);
        router.route("/olma").handler(guruxController::Olma);
        router.route("/lasetti").handler(guruxController::Lasetti);
        router.route("/gvazdika").handler(hayotiyController::Gvazdika);
        router.route("/kalibri").handler(hayotiyController::Kalibri);
        router.route("/qalam").handler(noadatiyController::Qalam);
        router.route("/ona").handler(noadatiyController::Ona);
        router.route("/bilet").handler(chiroyliController::Bilet);
        router.route("/astana").handler(chiroyliController::Astana);
        // Router router=Router.router(vertx);
        router.route().handler(BodyHandler.create());
        router.route("/ayirma/:son1/:son2").handler(ayirmaController::ayirma);
        router.route("/yigindi/:son1/:son2").handler(yigindiController::yigindi);
        router.route("/archa/:son1/:son2").handler(archaController::metod1);
        router.route("/dinamik/:son1").handler(dinamikController::metod1);
        router.route("stipendiya/uchlik").handler(stipendiyaController::Uchlik);
        router.route("stiendiya/tortlik").handler(stipendiyaController::Tortlik);
        router.route("stipendiya/beshlik").handler(stipendiyaController::Beshlik);

        router.route().handler(this::page404);
        return router;
    }
    public Router makeRouter(Vertx vertx){
        Router router=Router.router(vertx);
        router.route().handler(BodyHandler.create());
        router.route("/yigindi/:son1/:son2").handler(yigindiController::yigindi);
        router.route("/ayirma/:son1/:son2").handler(ayirmaController::ayirma);
        router.route("stipendiya/uchlik").handler(stipendiyaController::Uchlik);
        router.route("stipendiya/tortlik").handler(stipendiyaController::Tortlik);
        router.route("stipendiya/beshlik").handler(stipendiyaController::Beshlik);
        return router;
    }
    public void page404(RoutingContext routingContext){
        routingContext
                .response()
                .setStatusCode(404)
                .end("xato");
    }
}
